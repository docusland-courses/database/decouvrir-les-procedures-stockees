# Les procédures stockées

[Cours sur sql.sh](https://sql.sh/cours/procedure-stockee
)

```sql
DROP PROCEDURE afficher_race_selon_espece;

DELIMITER | -- Facultatif si votre délimiteur est toujours |
CREATE PROCEDURE afficher_race_selon_espece (IN p_espece_id INT)  
    -- Définition du paramètre p_espece_id
BEGIN
    SELECT id, nom, espece_id, prix 
    FROM Race
    WHERE espece_id = p_espece_id;  -- Utilisation du paramètre
END |
DELIMITER ;  -- On remet le délimiteur par défaut

CALL afficher_race_selon_espece(1);
SET @espece_id := 2;
CALL afficher_race_selon_espece(@espece_id);

```

Explications : 

 - Les paramètres IN: type par défault : paramètre entrant
 - Les paramètres OUT : paramètre sortant
 - Les paramètres INOUT : paramètre entrant et sortant
 - Pour définir une variable il faut utiliser le mot clé SET
 - Pour injecter une valeur dans une variable il faut utiliser le mot clé SELECT ... INTO
 - Il faut un bon nombre de champs avant et après le INTO
 - Les procédures stockées peuvent permettre de gagner en performance en diminuant les allers-retours entre le client et le serveur. Elles peuvent également aider à sécuriser une base de données et à s'assurer que les traitements sensibles sont toujours exécutés de la même manière.Fini le DELETE ou les UPDATE utilisateurs
 - Les procédures stockées rajoutent en charge serveur et ne sont pas automatiquement portable d'une SGBD vers une autre
 - Le symbole ‘@’ veut dire qu'il s'agit d'une variable utilisateur. S'il n'y a pas de symbole, il s'agit d'une variable locale. Le prefixe ‘@@’ concerne les variables système.
 - DECLARE n'initialise pas la variable. Lorsque vous déclarez une variable. Vous déclarez le nom,type et la valeur par défaut. Qui pourrait très bien être une expression.
- SET initialise la variable déclarée précédemment. Il n'est pas possible de SET sans DECLARE préalable.
- Évitez d'utiliser autant que possible les variables utilisateur dans les blocs. Elles sont définies d'une manière globales.


#### Exercice:

Nous allons réaliser une procédure stockée permettant de calculer le tarif total de quatre groupes donnés.

Partons de la structure et de la data présente dans ce projet. (Importer les deux fichiers sql de ce dépôt).

Prenons 4 id de groupes donnés, (75, 156, 42, 750) calculer le tarif total.

```sql
SET @tarif = 0;                   -- On initialise @tarif à 0
CALL calculer_cout (75, @tarif);  -- Réservation du groupe 75
CALL calculer_cout (156, @tarif);  -- Réservation du groupe 156
CALL calculer_cout (42, @tarif);  -- Réservation du groupe 42
CALL calculer_cout (750, @tarif);  -- Réservation du groupe 750
SELECT @tarif AS total;
-- SELECT sum(tarif) FROM groupes WHERE groupes.id_groupe IN (42, 75, 156, 750)
-- 138673281
```

#### Exercice avancé
Si vous êtes parvenu à réaliser cette procédure stockée, félicitations! 
Voici le même besoin mais rédigé d'une manière un peu plus complexe :

```sql
SET @tarif = 0;   
SET @groups := JSON_ARRAY(75, 156, 42, 750);
CALL group_cost(@groups, @tarif);
SELECT @tarif AS total;
```
